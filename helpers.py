from sqlalchemy.exc import IntegrityError
from wasd import db

# exceptions
class DBError(Exception): pass
class DoesNotExists(DBError): pass
class TooMuchResults(DBError): pass
class AlreadyExists(DBError): pass
class PermissionDenied(DBError): pass

class StandardDAOMixin:
    @classmethod
    def get(cls, **kwargs):
        ret = db.session.query(cls).filter_by(**kwargs).limit(2).all()
        if len(ret) > 1:
            raise TooMuchResults('More than one {} instance. Params: {}'.format(cls.__qualname__, kwargs))
        elif len(ret) == 0:
            raise DoesNotExists('There is no such {} instance. Params: {}'.format(cls.__qualname__, kwargs))
        else:
            return ret[0]

    SEARCH_LIMIT = 10

    @classmethod
    def search(cls, starts):
        if not hasattr(cls, 'name'):
            raise NotImplemented()
        return db.session.query(cls).filter(cls.name.startswith(starts)).order_by(cls.name)[:cls.SEARCH_LIMIT]

    def __init__(self):
        pass

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
        except IntegrityError:
            raise AlreadyExists('Already exists {} with such keys'.format(self.__class__.__name__))

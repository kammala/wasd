import logging


class Config:
    DEBUG = False
    LINKEDIN_CONSUMER_KEY = ''
    LINKEDIN_CONSUMER_SECRET = ''
    SECRET_KEY = 'empty'
    SQLALCHEMY_DATABASE_URI = 'sqlite://wasd.db'
    MAIL_SERVER = 'localhost'

    UPLOAD_DIR = 'media/'
    CELERY_BROKER_URL = 'sqla+sqlite:///celerydb.sqlite'


class Dev(Config):
    DEBUG = True

try:
    from config_production import Production
except ImportError:
    logging.warning('No production config')

import mimetypes
import os
from flask import render_template, Blueprint, request, current_app, redirect, url_for, flash, abort
from flask.ext.login import login_required, current_user
from flask.helpers import make_response
from werkzeug.utils import secure_filename

from helpers import DoesNotExists
from models import UserFile, SavedFile

main = Blueprint('main', __name__)


@main.route('/')
def index():
    return render_template('base.html')


@main.route('/new/', methods=['POST', 'GET'])
@login_required
def new():
    if current_user.has_exceed_max_files_count():
        flash('Exceed max file count', 'danger')
        return render_template('newfile.html')
    if request.method == 'POST':
        file = request.files['new']
        if file:
            old_file = current_user.get_file(file.filename)
            if old_file:
                old_file.delete()
            new_file = SavedFile()
            new_file.save()
            # _, ext = file.filename.rsplit('.', 1)
            new_filename = str(new_file.id)
            ext = 'data'
            file.save(os.path.join(current_app.config['UPLOAD_DIR'], '.'.join([new_filename, ext])))

            user_file = UserFile()
            user_file.file = new_file
            user_file.user = current_user
            user_file.original_filename = secure_filename(file.filename)
            user_file.save()
            from tasks import calculate_hashsum
            calculate_hashsum.delay(new_file.id)
            return redirect(url_for('main.listall'))
        else:
            flash('No file', 'warning')
    return render_template('newfile.html')


@main.route('/yours/')
@login_required
def listall():
    return render_template('listall.html')


@main.route('/delete/<int:file_id>')
@login_required
def delete(file_id):
    try:
        userfile = UserFile.get(user=current_user, file_id=file_id)
        filename = userfile.original_filename
        userfile.delete()
        flash("File '{}' was successfully deleted!".format(filename), 'success')
    except DoesNotExists:
        flash("No such file or you haven't rights to delete it.", 'warning')
    return redirect(url_for('main.listall'))


@main.route('/download/<userfile>/as/<filename>')
def directlink(userfile, filename):
    try:
        file = SavedFile.get(hashsum=userfile)
        with open(file.fs_processed_name, 'rb') as f:
            resp = make_response(f.read(), 200)
            resp.headers['Content-Type'], resp.headers['Content-Encoding'] = mimetypes.guess_type(filename)
            resp.headers['Content-Disposition'] = 'attachment; filename={}'.format(filename)
            return resp
    except DoesNotExists:
        abort(404)

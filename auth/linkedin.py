import random
from flask import Blueprint, url_for, request, redirect, flash, session
from flask.ext.login import login_user
from flask_oauthlib.client import OAuthException

from . import oauth
from helpers import DoesNotExists
from .models import LinkedInUser, User

linkedin = Blueprint('linkedin', __name__)
linkedin_api = oauth.remote_app('linkedin',
                                base_url='https://api.linkedin.com/',
                                request_token_url=None,
                                access_token_url='https://www.linkedin.com/uas/oauth2/accessToken',
                                authorize_url='https://www.linkedin.com/uas/oauth2/authorization',
                                app_key='LINKEDIN')


@linkedin_api.tokengetter
def get_linkedin_token():
    return {'access_token': session.get('linkedin_token')}


@linkedin.route('/login/')
def login():
    return linkedin_api.authorize(
        state=lambda: ''.join(random.sample('qwertyuiopasdfghjklzxcvbnm0123456789/*-+=_{}!@#$%^&()'*30, 30)),
        callback=url_for('linkedin.authorized', next=request.args.get('next') or request.referrer or None, _external=True))


@linkedin.route('/authorized/')
def authorized():
    next_url = request.args.get('next') or url_for('main.index')
    resp = linkedin_api.authorized_response()
    if resp is None:
        flash(u'You denied the request to sign in.', 'warning')
    elif isinstance(resp, OAuthException):
        flash(str(resp), 'danger')
    else:
        access_token = resp['access_token']
        session['linkedin_token'] = access_token
        resp = linkedin_api.get('/v1/people/~:(id,first-name,last-name)', data={'format': 'json'})
        if resp.status == 200:
            try:
                user = LinkedInUser.get(linkedin_user_id=resp.data['id']).user
            except DoesNotExists:
                linkedin_user_id = resp.data['id']
                user = User(first_name=resp.data['firstName'], last_name=resp.data['lastName'],
                            username='linkedin_' + linkedin_user_id)
                linkedin_user = LinkedInUser(linkedin_user_id=linkedin_user_id, user=user)
                linkedin_user.save()
                user.save()
            login_user(user)
        else:
            flash('Oops! An error has occured :(', 'danger')
    return redirect(next_url)

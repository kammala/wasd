import crypt
import random
from flask.ext.login import UserMixin
from flask.globals import current_app
from helpers import StandardDAOMixin

from wasd import db

METHOD = crypt.METHOD_SHA256


class User(db.Model, StandardDAOMixin, UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer(), primary_key=True)
    username = db.Column(db.String(32), unique=True)
    password = db.Column(db.String(256), nullable=True)
    salt = db.Column(db.String(100), nullable=True)
    first_name = db.Column(db.String(32), nullable=True)
    last_name = db.Column(db.String(32), nullable=True)

    def __init__(self, username, password=None, first_name=None, last_name=None):
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        if password:
            self.create_password(password)
        super().__init__()

    def create_password(self, pswd):
        """Creates POSIX crypted password."""
        self.password, self.salt = self.__crypt_password(pswd)

    @classmethod
    def __crypt_password(cls, pswd, salt=None):
        if not pswd:
            raise ValueError('Empty password not allowed')
        if not salt:
            salt = crypt.mksalt(METHOD)
        return crypt.crypt(pswd, salt), salt

    @classmethod
    def gen_password(cls, length=10):
        base_symbols = 'abcdefghijklmnopqrstuvwxyz1234567890+-*=?'
        return ''.join(random.sample(base_symbols*length, length))

    def check_password(self, pswd):
        return pswd and self.__crypt_password(pswd, self.salt)[0] == self.password

    def get_display_name(self):
        return self.username if not (self.first_name or self.last_name) else '{} {}'.format(self.first_name, self.last_name)

    def has_exceed_max_files_count(self):
        return len(self.files) >= current_app.config.get('MAX_FILE_COUNT', 100)

    def get_file(self, filename):
        from models import UserFile
        qry = db.session.query(UserFile).filter_by(user=self, original_filename=filename)
        if db.session.query(qry.exists()):
            return qry.first()
        else:
            return None


class LinkedInUser(db.Model, StandardDAOMixin):
    __tablename__ = 'linkedin_users'

    linkedin_user_id = db.Column(db.String(64), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))

    user = db.relationship('User', foreign_keys=[user_id], backref='linkedin_profile')

    __table_args__ = (
        db.UniqueConstraint('user_id', 'linkedin_user_id', name='ak__linkedin_users_uniq_mapping'),
    )

    def __init__(self, **kwargs):
        for arg, val in kwargs.items():
            setattr(self, arg, val)
        super(LinkedInUser, self).__init__()

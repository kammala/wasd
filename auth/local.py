import re
from flask import render_template, url_for, redirect, Blueprint, request, flash
from flask.ext.login import logout_user, login_user, login_required
from flask.ext.mail import Message

from .models import User
from wasd import mail
from helpers import DoesNotExists

local = Blueprint('local_auth', __name__)

EMAIL_VALIDATOR = re.compile(r'^\w+@\w+\.\w+$')

@local.route('/login/', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        try:
            user = User.get(username=request.form.get('username'))
        except DoesNotExists:
            flash('No such user', 'danger')
        else:
            if user.check_password(request.form.get('password')):
                login_user(user)
                return redirect(url_for('main.index'))
            else:
                flash('Invalid password', 'danger')
    return render_template('login.html')

@local.route('/logout/')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))

@local.route('/signup/', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        email = request.form.get('email').strip().lower()
        if EMAIL_VALIDATOR.match(email):
            try:
                user = User.get(username=email)
            except DoesNotExists:
                pswd = User.gen_password()
                user = User(username=email, password=pswd)
                message = Message('Registration', recipients=[email], sender=('WASD mail bot', 'wasd@kammala.ru'),
                                  html=render_template('mail/registration.html', username=email, password=pswd))
                mail.send(message)
                user.save()
                login_user(user)
                return redirect(url_for('main.index'))
            else:
                flash('User already exists', 'danger')
        else:
            flash('Incorrect email', 'danger')
    return render_template('signup.html')

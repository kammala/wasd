import os
from flask.globals import current_app
from helpers import StandardDAOMixin
from wasd import db


class SavedFile(db.Model, StandardDAOMixin):
    __tablename__ = 'files'

    id = db.Column(db.Integer, primary_key=True)
    hashsum = db.Column(db.String(32), nullable=True, index=True)

    @property
    def fs_unprocessed_name(self):
        filename = str(self.id)
        ext = 'data'
        return os.path.join(current_app.config['UPLOAD_DIR'], '.'.join([filename, ext]))

    @property
    def fs_processed_name(self):
        filename = self.hashsum
        ext = 'data'
        return os.path.join(current_app.config['UPLOAD_DIR'], 'processed', '.'.join([filename, ext]))


class UserFile(db.Model, StandardDAOMixin):
    __tablename__ = 'user_files'

    file_id = db.Column(db.Integer, db.ForeignKey('files.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    original_filename = db.Column(db.String(256))

    file = db.relationship('SavedFile', foreign_keys=[file_id], backref='userfiles')
    user = db.relationship('User', foreign_keys=[user_id], backref='files')

    __table_args__ = (
        db.PrimaryKeyConstraint('file_id', 'user_id', 'original_filename',
                                name='pk__user_files__user_file_name'),
    )

    def delete(self):
        owners_count = len(self.file.userfiles)
        saved_file = self.file
        super().delete()
        if owners_count == 1:
            os.remove(os.path.join(current_app.config['UPLOAD_DIR'],
                                   '.'.join([str(saved_file.id), 'data'])))
            saved_file.delete()

import os
import hashlib
from flask import current_app
from celery import Celery
from helpers import DoesNotExists

from models import SavedFile
from wasd import create_app, db

celery = Celery('wasd', broker='redis://localhost:6379/2')

@celery.task
def calculate_hashsum(file_id):
    app = create_app()
    with app.app_context():
        file = SavedFile.get(id=file_id)
        algo = hashlib.md5()
        with open(file.fs_unprocessed_name, 'rb') as f:
            algo.update(f.read())
            hashsum = algo.hexdigest()
        try:
            old = SavedFile.get(hashsum=hashsum)
        except DoesNotExists:
            file.hashsum = hashsum
            file.save()
            os.rename(file.fs_unprocessed_name, file.fs_processed_name)
        else:
            for uf in file.userfiles:
                uf.file = old
                db.session.add(uf)
            db.session.delete(file)
            db.session.commit()
            os.remove(file.fs_unprocessed_name)

import os

from flask import Flask
from flask.ext.login import LoginManager
from flask.ext.mail import Mail
from flask.ext.sqlalchemy import SQLAlchemy


db = SQLAlchemy()
login_manager = LoginManager()
mail = Mail()

@login_manager.user_loader
def load_user(userid):
    import auth.models
    import helpers
    try:
        return auth.models.User.get(id=userid)
    except helpers.DoesNotExists:
        pass


def create_app():
    app = Flask(__name__)
    app.config.from_object('config.{cfg}'.format(cfg=(os.environ.get('WASD_CONFIG') or 'dev').capitalize()))
    db.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    # blueprints
    from views import main
    from auth.linkedin import linkedin
    from auth.local import local
    app.register_blueprint(main)
    app.register_blueprint(linkedin, url_prefix='/linkedin')
    app.register_blueprint(local, url_prefix='/auth')

    return app

if __name__ == '__main__':
    app = create_app()
    app.run()

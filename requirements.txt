Flask>=0.10
Jinja2>=2.7
Flask-OAuthlib>=0.9
Flask-SQLAlchemy>=2.0
psycopg2>=2.6
Flask-Login>=0.2
Flask-Mail>=0.9
celery>=3.1
redis>=2.10
